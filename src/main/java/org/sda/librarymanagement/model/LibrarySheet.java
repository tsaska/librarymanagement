package org.sda.librarymanagement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "librarysheet")

public class LibrarySheet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sheetID")
	private int sheetID;
	
	@Column(name = "clientCNP")
	private String clientCNP;
	
	@Column(name = "bookID")
	private int bookID;
	
	@Column(name = "borrowingStartDate")
	private Date borrowingStartDate;
	
	@Column(name = "borrowingEndDate")
	private Date borrowingEndDate;

	public LibrarySheet() {

	}
	
	public int getSheetID() {
		return sheetID;
	}

	public void setSheetID(int sheetID) {
		this.sheetID = sheetID;
	}

	public String getClientCNP() {
		return clientCNP;
	}

	public void setClientCNP(String clientCNP) {
		this.clientCNP = clientCNP;
	}

	public int getBookID() {
		return bookID;
	}

	public void setBookID(int bookID) {
		this.bookID = bookID;
	}

	public Date getBorrowingStartDate() {
		return borrowingStartDate;
	}

	public void setBorrowingStartDate(Date borrowingStartDate) {
		this.borrowingStartDate = borrowingStartDate;
	}

	public Date getBorrowingEndDate() {
		return borrowingEndDate;
	}

	public void setBorrowingEndDate(Date borrowingEndDate) {
		this.borrowingEndDate = borrowingEndDate;
	}

}
