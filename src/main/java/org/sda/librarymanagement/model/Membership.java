package org.sda.librarymanagement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "membership")

public class Membership {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "membershipID")
	private int membershipID;
	
	@Column(name = "clientCNP")
	private String clientCNP;
	
	@Column(name = "isLibrarian")
	private boolean isLibrarian;
	
	@Column(name = "isPremium")
	private boolean isPremium;
	
	@Column(name = "startDate")
	private Date startDate;
	
	@Column(name = "endDate")
	private Date endDate;

	public Membership() {

	}

	public Membership(String clientCNP, boolean isLibrarian, boolean isPremium, Date startDate, Date endDate) {
		this.clientCNP = clientCNP;
		this.isLibrarian = isLibrarian;
		this.isPremium = isPremium;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public int getMembershipID() {
		return membershipID;
	}

	public void setMembershipID(int membershipID) {
		this.membershipID = membershipID;
	}

	public String getClientCNP() {
		return clientCNP;
	}

	public void setClientCNP(String clientCNP) {
		this.clientCNP = clientCNP;
	}

	public boolean isLibrarian() {
		return isLibrarian;
	}

	public void setLibrarian(boolean isLibrarian) {
		this.isLibrarian = isLibrarian;
	}

	public boolean isPremium() {
		return isPremium;
	}

	public void setPremium(boolean isPremium) {
		this.isPremium = isPremium;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
