package org.sda.librarymanagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "books")

public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bookID")
	private int bookID;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "isBorrowable")
	private boolean isBorrowable;

	public Book() {

	}

	public Book(String title, boolean isBorrowable) {
		this.title = title;
		this.isBorrowable = isBorrowable;
	}

	public int getBookID() {
		return bookID;
	}

	public void setBookID(int bookID) {
		this.bookID = bookID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isBorrowable() {
		return isBorrowable;
	}

	public void setBorrowable(boolean isBorrowable) {
		this.isBorrowable = isBorrowable;
	}

	@Override
	public String toString() {
		return "Book [bookID=" + bookID + ", title=" + title + ", isBorrowable=" + isBorrowable + "]";
	}

}
