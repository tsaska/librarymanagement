package org.sda.librarymanagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.sda.librarymanagement.model.Book;
import org.sda.librarymanagement.model.Client;

public class App {
	public static void main(String[] args) {
/*	//testarea conexiunii:
		String jdbcUrl = "jdbc:mysql://localhost:3306/library_management?useSSL=false&serverTimezone=UTC";
		String user = "root";
		String pass = "Java2018";
		try {
			System.out.println("Connecting to Database: " + jdbcUrl);
			Connection myConn = DriverManager.getConnection(jdbcUrl, user, pass);
			System.out.println("Connection successful");
		} catch (Exception exc) {
			exc.printStackTrace();
		}*/
		
		SessionFactory factory = new Configuration().configure().addAnnotatedClass(Client.class).buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		
		/*try {
			System.out.println("Creating a new client object...");
			Client client1 = new Client("1801112123333", "Andrei", "Ion", "861123", "iandrei@gmail.com", "str. Cucului 12");
			Client client2 = new Client("1811111122233", "Paul", "Duta", "862333", "dpaul@gmail.com", "str. Razoare 12");
			Client client3 = new Client("1822233344455", "Ioan", "Man", "861444", "mioan@gmail.com", "str. Viitorului 12");
			Client client4 = new Client("1833300221222", "Raul", "Pop", "861235", "praul@gmail.com", "str. Trecutului 12");
			Client client5 = new Client("1840022223333", "Ioana", "Salanta", "411563", "sioana@gmail.com", "str. Clujului 12");
			Client client6 = new Client("1852222333333", "Melania", "Man", "125333", "mmelania@gmail.com", "str. Bucuresti 12");
			Client client7 = new Client("1866555555555", "Anamaria", "Strut", "111222", "sana@gmail.com", "str. Brasov 12");
			Client client8 = new Client("1878888888888", "Flavia", "Catuna", "632145", "cflavia@gmail.com", "str. Calea Turzii 12");
			Client client9 = new Client("1889999999999", "Dana", "Batentian", "784545", "bdana@gmail.com", "str. Taietura Turcului 12");
		
			session.beginTransaction();
			System.out.println("Saving the clients...");
			session.save(client1);
			session.save(client2);
			session.save(client3);
			session.save(client4);
			session.save(client5);
			session.save(client6);
			session.save(client7);
			session.save(client8);
			session.save(client9);
			
			session.getTransaction().commit();
			
			System.out.println("Done!");
			
			
		}
		finally {
			factory.close();
		}*/
		
/*		try {
			System.out.println("Creating a new book object...");
			Book book1 = new Book("Rascoala", true);
			Book book2 = new Book("Ion", true);
			Book book3 = new Book("Baltagul", false);
			Book book4 = new Book("Morometii vol.1", true);
			Book book5 = new Book("Morometii vol.2", true);
			Book book6 = new Book("Cum sa te lasi de fumat", true);
			Book book7 = new Book("Lord of the Rings", true);
			Book book8 = new Book("Java from A to Z", false);
			Book book9 = new Book("Thinking in JAVA", false);
		
			session.beginTransaction();
			System.out.println("Saving the clients...");
			session.save(book1);
			session.save(book2);
			session.save(book3);
			session.save(book4);
			session.save(book5);
			session.save(book6);
			session.save(book7);
			session.save(book8);
			session.save(book9);
			
			session.getTransaction().commit();
			
			System.out.println("Done!");
			
			
		}
		finally {
			factory.close();
		}*/
		
		try {
			session.beginTransaction();
			
			List<Client> theClients = session.createQuery("from Client").getResultList();
			displayClients(theClients);
			
			
			theClients = session.createQuery("from Client clients where clients.lastName='Man'").getResultList();
			System.out.println("\nClientii cu numele Man:\n");
			displayClients(theClients);
			
			theClients = session.createQuery("from Client clients where clients.lastName='Pop' OR clients.firstName='Dana'").getResultList();
			System.out.println("\nClientii selectati sunt:\n");
			displayClients(theClients);
			
			List<Book> theBooks = session.createQuery("from Book").getResultList();
			System.out.println("\nLista Cartilor:\n");
			for(Book books : theBooks) {
				System.out.println(books);
			}
			
			theBooks = session.createQuery("from Book books where books.isBorrowable = false").getResultList();
			System.out.println("\nCartile care nu se imprumuta:\n");
			for(Book books : theBooks) {
				System.out.println(books);
			}
			
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}
		finally {
			factory.close();
		}
	}

	private static void displayClients(List<Client> theClients) {
		for(Client clients : theClients) {
			System.out.println(clients);
		}
	}
}
